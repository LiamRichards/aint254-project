﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GoalSystem : MonoBehaviour {

    private List<GameObject> m_pointList = new List<GameObject>();

    public Component[] points;

    public int pointsLeft;

    [SerializeField]
    private GameObject Bike;

    [SerializeField]
    private GameObject Panel;

    [SerializeField]
    private GameObject m_LightBeam;

    [SerializeField]
    private GameObject m_timer;

    // Use this for initialization
    void Start()
    {

        CheckNumHit();
        ShowNextGoal();
    }

    // Update is called once per frame
    void Update()
    {


    }

    public void ShowNextGoal()
    {
        CheckNumHit();
        if(pointsLeft != 0)
        {
            int currentGoal = points.Length - pointsLeft;

            points[currentGoal].GetComponent<BoxCollider>().enabled = true;
            points[currentGoal].GetComponent<Light>().enabled = true;
            m_LightBeam.transform.position = points[currentGoal].transform.position;

        }
    }

    public void CheckNumHit()
    {
        points = GetComponentsInChildren<GoalBehaviour>();

        //m_pointList.Clear();
        pointsLeft = 0;

        foreach (GoalBehaviour point in points)
        {
            if (!point.m_hit)
            {
                pointsLeft++;
            }
        }

        Debug.Log(pointsLeft);

        if (pointsLeft == 0)
        {
            LevelComplete();
        }
    }

    void LevelComplete()
    {
        Bike.GetComponent<MoveBike>().enabled = false;
        m_timer.GetComponent<Timer>().StopTimer();
        Panel.SetActive(true);

        Debug.Log("Nep");
    }
}
