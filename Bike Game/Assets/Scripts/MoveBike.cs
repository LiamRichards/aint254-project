﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MoveBike : MonoBehaviour {


    private Rigidbody m_Rigidbody;
    private Transform m_transform;

    [SerializeField] private GameObject m_FWheel;
    [SerializeField] private GameObject m_BWheel;
    private WheelCollider m_FWheelCol;
    private WheelCollider m_BWheelCol;

    [SerializeField] private GameObject centerOfMass;
    [SerializeField] private float m_SlipLimit;

    [Range(0, 1)][SerializeField] private float m_TractionControl; // 0 is no traction control, 1 is full interference
    [Range(0, 1)][SerializeField] private float m_SteerHelper; // 0 is raw physics , 1 the car will grip in the direction it is facing

    [SerializeField] private float m_FullTorqueOverAllWheels;
    private float m_MaxHandbrakeTorque;
    private float m_CurrentTorque;
    public float CurrentSpeed { get { return m_Rigidbody.velocity.magnitude; } }
    [SerializeField] private float m_BrakeTorque;
    [SerializeField] private float m_ReverseTorque;
    [SerializeField] private float m_Topspeed = 500;

    public float BrakeInput { get; private set; }
    public float AccelInput { get; private set; }

    private float m_OldRotation;

    [SerializeField] private float m_Downforce = 100f;
    [SerializeField] private float m_MaximumSteerAngle;
    private float m_SteerAngle;

    [SerializeField] private GameObject m_SpawnPoint;

    [SerializeField] private float m_JumpForce = 100f;


    [SerializeField] private AudioClip m_bikeBell;
    private AudioSource m_source;

    // Use this for initialization
    void Start () {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_transform = transform;
        m_FWheelCol = m_FWheel.GetComponent<WheelCollider>();
        m_BWheelCol = m_BWheel.GetComponent<WheelCollider>();
        m_Rigidbody.centerOfMass = m_Rigidbody.centerOfMass + centerOfMass.transform.localPosition;

        m_MaxHandbrakeTorque = float.MaxValue;

        m_Rigidbody = GetComponent<Rigidbody>();
        m_CurrentTorque = m_FullTorqueOverAllWheels - (m_TractionControl * m_FullTorqueOverAllWheels);

        m_source = GetComponent<AudioSource>();
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update () {

        Move(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), Input.GetAxis("Vertical"), 0);
        //m_transform.rotation = Quaternion.Slerp(m_transform.localRotation, Quaternion.Euler(m_transform.localRotation.eulerAngles.x, m_transform.localRotation.eulerAngles.y, 0), Time.deltaTime * 10);

        if (m_transform.position.y < -100 || Input.GetKeyDown(KeyCode.R))
        {
            m_transform.position = m_SpawnPoint.transform.position;
            m_Rigidbody.angularVelocity = Vector3.zero;
            m_Rigidbody.velocity = Vector3.zero;
            m_Rigidbody.transform.rotation = Quaternion.Euler(Vector3.zero);
        }

        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
            m_Rigidbody.AddRelativeForce(new Vector3(0, 1, 0) * m_JumpForce, ForceMode.Impulse);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {            
            m_source.PlayOneShot(m_bikeBell,1);
        }

        if(Input.GetKeyDown(KeyCode.F5))
        {
            SceneManager.LoadScene(1);
            
        }
        //print(m_Rigidbody.velocity);
    }

    public void Move(float steering, float accel, float footbrake, float handbrake) {

        steering = Mathf.Clamp(steering, -1, 1);
        AccelInput = accel = Mathf.Clamp(accel, 0, 1);
        BrakeInput = footbrake = -1 * Mathf.Clamp(footbrake, -1, 0);
        handbrake = Mathf.Clamp(handbrake, 0, 1);

        m_SteerAngle = steering * m_MaximumSteerAngle;
        m_FWheelCol.steerAngle = m_SteerAngle;
        if(steering == 0)
            m_transform.rotation = Quaternion.Slerp(m_transform.localRotation, Quaternion.Euler(m_transform.localRotation.eulerAngles.x, m_transform.localRotation.eulerAngles.y, 0), Time.deltaTime * 10);

        SteerHelper();
        ApplyDrive(accel, footbrake);
        CapSpeed();

        if (handbrake > 0f)
        {
            var hbTorque = handbrake * m_MaxHandbrakeTorque;
            m_BWheelCol.brakeTorque = hbTorque;
        }
        AddDownForce();
        TractionControl();


       
    }

    private void CapSpeed()
    {
        float speed = m_Rigidbody.velocity.magnitude;
                        
                if (speed > m_Topspeed)
                    m_Rigidbody.velocity = m_Topspeed * m_Rigidbody.velocity.normalized;   
                
                  
    }



    private void ApplyDrive(float accel, float footbrake)
    {

        float thrustTorque;

        thrustTorque = accel * (m_CurrentTorque / 2f);
        m_BWheelCol.motorTorque  = thrustTorque;
        m_FWheelCol.motorTorque = thrustTorque;

        if (Vector3.Angle(transform.forward, m_Rigidbody.velocity) < 50f)
        {
            m_FWheelCol.brakeTorque = m_BrakeTorque * footbrake;
        }

        else if (footbrake > 0.1)
        {
            m_BWheelCol.brakeTorque = 0f;
            m_BWheelCol.motorTorque = -m_ReverseTorque * footbrake;
        }
    }

    

    private void AdjustTorque(float forwardSlip)
    {
        if (forwardSlip >= m_SlipLimit && m_CurrentTorque >= 0)
        {
            m_CurrentTorque -= 10 * m_TractionControl;
        }
        else
        {
            m_CurrentTorque += 10 * m_TractionControl;
            if (m_CurrentTorque > m_FullTorqueOverAllWheels)
            {
                m_CurrentTorque = m_FullTorqueOverAllWheels;
            }
        }
    }

    private void TractionControl()
    {
        WheelHit wheelHit;

        m_FWheelCol.GetGroundHit(out wheelHit);
        AdjustTorque(wheelHit.forwardSlip);

        m_BWheelCol.GetGroundHit(out wheelHit);
        AdjustTorque(wheelHit.forwardSlip);
    }

    private void SteerHelper()
    {
        for (int i = 0; i < 4; i++)
        {
            WheelHit wheelhit;
            m_FWheelCol.GetGroundHit(out wheelhit);
            if (wheelhit.normal == Vector3.zero)
                return; // wheels arent on the ground so dont realign the rigidbody velocity

            m_BWheelCol.GetGroundHit(out wheelhit);
            if (wheelhit.normal == Vector3.zero)
                return; // wheels arent on the ground so dont realign the rigidbody velocity

        }

        // this if is needed to avoid gimbal lock problems that will make the car suddenly shift direction
        if (Mathf.Abs(m_OldRotation - transform.eulerAngles.y) < 10f)
        {
            var turnadjust = (transform.eulerAngles.y - m_OldRotation) * m_SteerHelper;
            Quaternion velRotation = Quaternion.AngleAxis(turnadjust, Vector3.up);
            m_Rigidbody.velocity = velRotation * m_Rigidbody.velocity;
        }
        m_OldRotation = transform.eulerAngles.y;
    }


    // this is used to add more grip in relation to speed
    private void AddDownForce()
    {
        m_Rigidbody.AddForce(-transform.up * m_Downforce * m_Rigidbody.velocity.magnitude);
    }

    bool IsGrounded()
    {
        return Physics.Raycast(m_BWheel.transform.position, -Vector3.up, m_BWheelCol.bounds.extents.y + 1.5f);
    }
}
