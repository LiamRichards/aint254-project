﻿using UnityEngine;
using System.Collections;

public class PointBehaviour : MonoBehaviour {

    public bool m_hit = false;
    private AudioSource m_source;
    [SerializeField]
    private AudioClip m_sound;

    // Use this for initialization
    void Awake()
    {
        m_source = GetComponent<AudioSource>();
    }
	// Update is called once per frame
	void Update () {
        transform.Rotate(1, 1, 1);
	}

    void OnTriggerEnter()
    {
        m_source.PlayOneShot(m_sound, 1);
        m_hit = true;

        //gameObject.SetActive(false);
        gameObject.GetComponent<BoxCollider>().enabled = false;
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        PointSystem temp = GetComponentInParent<PointSystem>();
        temp.CheckNumHit();
    }
}
