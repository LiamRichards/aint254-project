﻿using UnityEngine;
using System.Collections;

public class GoalBehaviour : MonoBehaviour {

    public bool m_hit = false;
    private AudioSource m_source;
    [SerializeField]
    private AudioClip m_sound;

    // Use this for initialization
    void Awake()
    {
        m_source = GetComponent<AudioSource>();
    }

    void OnTriggerEnter()
    {
        m_source.PlayOneShot(m_sound, 1);
        m_hit = true;

        gameObject.GetComponent<BoxCollider>().enabled = false;
        //gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<Light>().enabled = false;
        

        GoalSystem temp = GetComponentInParent<GoalSystem>();
        temp.CheckNumHit();
        temp.ShowNextGoal();
    }
}
