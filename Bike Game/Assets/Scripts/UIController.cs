﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour {

    [SerializeField]
    private GameObject m_menu;

    [SerializeField]
    private GameObject m_timer;

	// Use this for initialization
	void Start () {
        m_timer.GetComponent<Timer>().StartTimer();
	}
	
	// Update is called once per frame
	void Update () {
	
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            print("NEP");
            if(!m_menu.activeSelf)
            {               
                m_menu.SetActive(true);
                Time.timeScale = 0;
            }
            else
            {
                m_menu.SetActive(false);
                Time.timeScale = 1;
            }
        }

        //if(Input.GetKeyDown(KeyCode.Escape))
        //{
        //    m_timer.GetComponent<Timer>().ResetTimer();
        //}

	}
}
