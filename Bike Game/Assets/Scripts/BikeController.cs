﻿using UnityEngine;
using System.Collections;

public class BikeController : MonoBehaviour
{

    private Rigidbody m_rigidbody;
    private Transform m_transform;

    [SerializeField]
    private GameObject m_FWheel;
    private WheelCollider m_FWheelCol;

    [SerializeField]
    private GameObject m_BWheel;
    private MeshCollider m_BWheelCol;

    [SerializeField]
    private GameObject m_FFrame;

    [SerializeField]
    private GameObject m_centerOfMass;

    [SerializeField]
    private GameObject m_rotationPoint;

    [SerializeField]
    private float m_speed = 5.0f;
    private Vector3 m_maxSpeed;

    private float m_wait = 0;

    // Use this for initialization
    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_transform = transform;
        m_BWheelCol = m_BWheel.GetComponent<MeshCollider>();
        m_rigidbody.centerOfMass = m_rigidbody.centerOfMass + m_centerOfMass.transform.localPosition;
        m_maxSpeed = new Vector3(m_speed, m_speed, m_speed);

        m_rigidbody.angularVelocity = Vector3.zero;
        m_rigidbody.velocity = Vector3.zero;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ForwardsandBack();

        if(m_transform.position.y < -100)
        {
            m_transform.position = new Vector3(10, 10, 10);
            m_rigidbody.angularVelocity = Vector3.zero;
            m_rigidbody.velocity = Vector3.zero;

        }
        //m_rigidbody.angularVelocity = Vector3.zero;
    }

    void ForwardsandBack()
    {
        if (Input.GetAxis("Vertical") > 0 && IsGrounded())
        {
            m_wait = 0f;

            if(m_rigidbody.velocity.x < m_maxSpeed.x && m_rigidbody.velocity.x > -m_maxSpeed.x)
            {
                if(m_rigidbody.velocity.z < m_maxSpeed.z && m_rigidbody.velocity.z > -m_maxSpeed.z)
                {
                    m_rigidbody.AddRelativeForce(new Vector3(0, 0.1f, 1) * Time.deltaTime * 10000);
                    //m_rigidbody.AddRelativeTorque(new Vector3(1, 0, 0) * 1000);

                    //m_rigidbody.transform.position = new Vector3(m_transform.position.x, m_transform.position.y, m_transform.position.z + 0.1f);
                    //m_rigidbody.velocity = new Vector3(0, 0.1f, 1) * Time.deltaTime * 1000;

                    //float x = 0;
                    //float y = 0f;
                    //float z = 0;

                    //if(m_rigidbody.rotation.y < 90)
                    //{
                    //    z = (Mathf.Cos(Mathf.DeltaAngle(0, m_rigidbody.rotation.y)));
                        
                    //    x = (Mathf.Sin(Mathf.DeltaAngle(0, m_rigidbody.rotation.y)));
                    //}

                    //if(m_rigidbody.rotation.y > 90)
                    //{
                    //    z = (Mathf.Cos(Mathf.DeltaAngle(180, m_rigidbody.rotation.y)));

                    //    x = (Mathf.Sin(Mathf.DeltaAngle(180, m_rigidbody.rotation.y)));
                    //}


                    //Vector3 wantedVector = new Vector3(x,y,z);

                    //print(wantedVector);

                    //m_rigidbody.velocity = wantedVector * Time.deltaTime * 1000;
                }
            }

            ForwardSteering();
            
        }
        else if (Input.GetAxis("Vertical") < 0 && IsGrounded())
        {
            m_wait = 0f;

            if (m_rigidbody.velocity.x < 0.5 * m_maxSpeed.x && m_rigidbody.velocity.x > 0.5 * -m_maxSpeed.x)
            {
                if (m_rigidbody.velocity.z < 0.5 * m_maxSpeed.z && m_rigidbody.velocity.z > 0.5 * -m_maxSpeed.z)
                {
                    m_rigidbody.AddRelativeForce(new Vector3(0, 0.1f, -1) * Time.deltaTime * 10000);
                }
            }

            BackwardSteering();

        }
        else
        {
            m_FWheel.transform.localRotation = Quaternion.Slerp(m_FWheel.transform.localRotation, Quaternion.Euler(0, m_FWheel.transform.localRotation.y - 90, 0), Time.deltaTime * 10);
            m_transform.rotation = Quaternion.Slerp(m_transform.localRotation, Quaternion.Euler(m_transform.localRotation.eulerAngles.x, m_transform.localRotation.eulerAngles.y, 0), Time.deltaTime * 20);


            m_wait += Time.deltaTime;

            ForwardSteering();
        }

        if (Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
            m_rigidbody.AddRelativeForce(new Vector3(0,1,0) * 2000);
            
        }

    }

    void ForwardSteering()
    {
        if (Input.GetAxis("Horizontal") > 0)
        {

            //m_transform.Rotate(new Vector3(0,1,0), 1);
            m_transform.RotateAround(m_rotationPoint.transform.position, new Vector3(0,1,0) ,1);
            //m_FWheel.transform.localRotation = Quaternion.Slerp(m_FWheel.transform.localRotation, Quaternion.Euler(0, m_FWheel.transform.localRotation.y - 90 + 10, 0), Time.deltaTime * 5);
            //m_transform.rotation = Quaternion.Slerp(m_transform.localRotation, Quaternion.Euler(m_transform.localRotation.x, m_transform.localRotation.y, m_transform.localRotation.z - 10), Time.deltaTime * 5);
            
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            //m_transform.Rotate(new Vector3(0, 1, 0), -1);
            m_transform.RotateAround(m_rotationPoint.transform.position, new Vector3(0, 1, 0), -1);
            //m_FWheel.transform.localRotation = Quaternion.Slerp(m_FWheel.transform.localRotation, Quaternion.Euler(0, m_FWheel.transform.localRotation.y - 90 - 10 ,0) , Time.deltaTime * 5);

        }
        else
        {
            m_FWheel.transform.localRotation = Quaternion.Slerp(m_FWheel.transform.localRotation, Quaternion.Euler(0, m_FWheel.transform.localRotation.y - 90, 0), Time.deltaTime * 10);
            m_transform.rotation = Quaternion.Slerp(m_transform.localRotation, Quaternion.Euler(m_transform.localRotation.eulerAngles.x, m_transform.localRotation.eulerAngles.y, 0), Time.deltaTime * 10);


        }
    }

    void BackwardSteering()
    {
        if (Input.GetAxis("Horizontal") > 0)
        {
            m_transform.Rotate(new Vector3(0, 1, 0), -1);
            m_FWheel.transform.localRotation = Quaternion.Slerp(m_FWheel.transform.localRotation, Quaternion.Euler(0, m_FWheel.transform.localRotation.y - 90 + 30, 0), Time.deltaTime * 5);


        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            m_transform.Rotate(new Vector3(0, 1, 0), 1);
            m_FWheel.transform.localRotation = Quaternion.Slerp(m_FWheel.transform.localRotation, Quaternion.Euler(0, m_FWheel.transform.localRotation.y - 90 - 30, 0), Time.deltaTime * 5);


        }
        else
        {
            m_FWheel.transform.localRotation = Quaternion.Slerp(m_FWheel.transform.localRotation, Quaternion.Euler(0, m_FWheel.transform.localRotation.y - 90, 0), Time.deltaTime * 10);
            m_transform.rotation = Quaternion.Slerp(m_transform.localRotation, Quaternion.Euler(m_transform.localRotation.eulerAngles.x, m_transform.localRotation.eulerAngles.y, 0), Time.deltaTime * 10);

        }
    }

    bool IsGrounded()
    {
        return Physics.Raycast(m_BWheel.transform.position, -Vector3.up, m_BWheelCol.bounds.extents.y + 0.2f);
    }
}
