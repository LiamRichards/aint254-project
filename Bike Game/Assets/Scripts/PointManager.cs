﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PointManager : MonoBehaviour {

    [SerializeField]
    private GameObject pointSystem;

    Text pointText;

    int pointsLeft;

    // Use this for initialization
    void Start () {
        pointText = GetComponent<Text>();

        pointsLeft = pointSystem.GetComponent<PointSystem>().pointsLeft;
	}
	
	// Update is called once per frame
	void Update () {
        pointsLeft = pointSystem.GetComponent<PointSystem>().pointsLeft;
        pointText.text = "Points Left: " + pointsLeft;
	}
}
