﻿using UnityEngine;
using System.Collections;

public class LookAtCamara : MonoBehaviour {

    [SerializeField]
    private GameObject m_target;

    [SerializeField]
    private Vector3 offset;

    [SerializeField]
    private float damping = 1;

    private Transform m_transform;

	// Use this for initialization
	void Start () {
        m_transform = gameObject.transform;
        m_transform.position = m_target.transform.position;
        //offset = m_transform.position - m_target.transform.position;
        
	}
	
	// Update is called once per frame
	void Update () {

        //Vector3 desiredPosition = m_target.transform.position + offset;
        //Vector3 position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * damping);
        //transform.position = position;
        //transform.LookAt(m_target.transform);

        float desiredAngle = m_target.transform.eulerAngles.y;
        Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);
        m_transform.position = m_target.transform.position + (rotation * offset);

        m_transform.LookAt(m_target.transform);

        //Debug.Log(m_target.transform);
    }
}
