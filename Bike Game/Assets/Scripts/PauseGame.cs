﻿using UnityEngine;
using System.Collections;

public class PauseGame : MonoBehaviour {

	public void TogglePause()
    {
        if(Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        else
        {
            Time.timeScale = 0;
        }
    }
}
