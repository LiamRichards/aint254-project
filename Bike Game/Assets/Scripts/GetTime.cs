﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetTime : MonoBehaviour {

    [SerializeField]
    private GameObject m_timer;

    private double time;
	// Use this for initialization


    // Update is called once per frame
    void Update()
    {
        time = System.Math.Round(m_timer.GetComponent<Timer>().m_time, 2);
        GetComponent<Text>().text = "Time: " + time.ToString();
    }

}
